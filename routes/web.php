<?php

use Illuminate\Support\Facades\Route;
use App\Http\Middleware\isAdmin;
use App\Http\Controllers\Admin\EmployeeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    return redirect()->route('login');
});


Route::group(['prefix' => 'admin'], function () {

    Route::get('/', 'App\Http\Controllers\Admin\AdminController@index')->name('login');
    Route::post('/post-login', 'App\Http\Controllers\Admin\AdminController@postLogin')->name('postLogin');
    Route::get('/logout', 'App\Http\Controllers\Admin\AdminController@logout')->name('adminLogout');

    Route::middleware([isAdmin::class])->group(function () {

        Route::get('/dashboard', 'App\Http\Controllers\Admin\AdminController@dashboard')->name('dashboard');

        Route::get('/employees', 'App\Http\Controllers\Admin\EmployeeController@index')->name('employees.index');
        Route::get('/employees/create', 'App\Http\Controllers\Admin\EmployeeController@create')->name('employees.create');
        Route::post('employees/store','App\Http\Controllers\Admin\EmployeeController@store')->name('employees.store');
        Route::get('/employee/{id}/edit','App\Http\Controllers\Admin\EmployeeController@edit')->name('employees.edit');
        Route::post('/employees/{id}/update','App\Http\Controllers\Admin\EmployeeController@update')->name('employees.update');
        Route::get('/employee/{id}/delete','App\Http\Controllers\Admin\EmployeeController@destroy')->name('employees.destroy');
        Route::get('/employees/export', 'App\Http\Controllers\Admin\EmployeeController@export')->name('employees.export');
        Route::post('import', [EmployeeController::class, 'import'])->name('import');

    });
});

