<?php

namespace App\Imports;

use App\Models\Employee;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Http\Controllers\Admin\Excel;

use App\Models\User;
use Maatwebsite\Excel\Concerns\WithHeadingRow;


class EmployeeImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
        return new Employee([
            'first_name'     => $row['first_name'],
            'last_name'     => $row['last_name'],
            'email'     => $row['email'],
            'hobbies'     => $row['hobbies'],
            'gender'     => $row['gender'],
            'image'     => $row['image'],
        ]);
    }
}
