<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Employee;
use Validator,File,DB,Auth,DataTables;
use App\Helpers\ImageUploadHelper;
use Maatwebsite\Excel\Facades\Excel;
use App\imports\EmployeeImport;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
       
        if ($request->ajax()) {

            $employees= Employee::orderBy("id","DESC")->get();

            return DataTables::of($employees)
                ->addIndexColumn()
                ->addColumn('action', function($row){   
                    $btn = '<div><a href="'.route('employees.edit',['id'=>$row->id]).'"><i class="fa fa-edit edi" aria-hidden="true" ></i>
                    </a>&nbsp;<a class="ml-2" onclick=confirmationPopup("'.route('employees.destroy',['id'=>$row->id]).'") href="javascript:void(0)">
                    <i class="fa fa-trash del" aria-hidden="true"></i></a></div>';
            return $btn;
                })
                ->addColumn('image', function($row) {
                    return '<div class="d-inline-block align-middle">
                                <img src="' . url($row->image) . '"
                                        class="img-40 m-r-15 rounded-circle align-center" style="max-width:50px; max-height:auto;"/>
                            </div>';
                })
                ->addColumn('gender', function($row) {
                    if($row->gender == 'male') {
                        return '<span class="badge badge-success">Male</span>';
                    } else {
                        return '<span class="badge badge-info">Female</span>';
                    }
                })
                ->rawColumns(['action', 'image', 'gender'])
                ->make(true);
            }
            return view('admin.employees.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.employees.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = null;
        if ($request->hasFile('image')) {
            $image = ImageUploadHelper::imageUpload($request->file('image'));
        }
        try {
            $employees = new Employee();
            $employees->first_name = $request['first_name'];
            $employees->last_name = $request['last_name'];
            $employees->email = $request['email'];
            $employees->hobbies = isset($request['hobby']) ? implode(', ', $request['hobby']) : '';
            $employees->gender = $request['gender'];
            $employees->image = $image;
            $employees->save();    
        }   
        catch (Exception $e) {
            return redirect()->route('employees.index')->with('error','Something went wrong. Please try again later ...');
        }  
        return redirect()->route('employees.index')->with('info','Employee Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::find($id);
        return view('admin.employees.edit',compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $image = null;
        if ($request->hasFile('image')) {
            $image = ImageUploadHelper::imageUpload($request->file('image'));
        }
        try {
        Employee::where('id', $id)->update([
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'email' => $request['email'],
            'hobbies' => isset($request['hobby']) ? implode(', ', $request['hobby']) : '',
            'gender' => $request['gender'],
            'image' => $image,
        ]); 
        }   
        catch (Exception $e) {
            return redirect()->route('employees.index')->with('error','Something went wrong. Please try again later ...');
        }  
        return redirect()->route('employees.index')->with('info','Employee Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = Employee::find($id);
        $employee->delete();
        // return redirect()->route('employees.index');
        return response()->json(["success"=>true,"message"=>"Successfully delete"]);
    }

    public function import() 
    {
        Excel::import(new EmployeeImport,request()->file('file'));
        return back();
    }
}
