
<!DOCTYPE html>
<html lang="en">
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>Login | Employee Panel</title>
		<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
		<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" rel="stylesheet" type="text/css"/>
		<link href="{{ asset('assets/admin/css/font-awesome.min.css') }}" rel="stylesheet" />
		<link href="{{ asset('assets/admin/css/material-dashboard.min.css') }}" rel="stylesheet" />
		<link href="{{ asset('assets/admin/css/material-dashboard.min.css') }}" rel="stylesheet"/>
		<link href="{{ asset('assets/admin/demo/demo.css') }}" rel="stylesheet"/>
	</head>
	<body class="off-canvas-sidebar">
		<!-- Start Navbar -->
		<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top text-white">
			<div class="container">
				<div class="navbar-wrapper">
					<a class="navbar-brand font-weight-bold" href="javascript:void(0)">Employee Panel</a>
				</div>
				<button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
					<span class="sr-only">Toggle browsing</span>
					<span class="navbar-toggler-icon icon-bar"></span>
					<span class="navbar-toggler-icon icon-bar"></span>
					<span class="navbar-toggler-icon icon-bar"></span>
				</button>

			</div>
		</nav>
	  	<!-- End Navbar -->
		<div class="wrapper wrapper-full-page">
			<div class="page-header login-page header-filter" filter-color="black" style="background-image: url({{url('assets/admin/img/login.jpg')}}); background-size: cover; background-position: top center;">
				<div class="container">
					<div class="row">
						<div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
							<form class="form" method="post" action="{{route('postLogin')}}">
								{{-- <form class="form" method="post" action="#"> --}}
								@csrf
								<div class="card card-login card-hidden">
									<div class="card-header card-header-rose headcolor text-center">
										<h4 class="card-title samplecolor">LOG IN</h4>
									</div>
									<div class="card-body login-form">
										@if(session('error'))
										<div class="text-center">
											<span class="text-danger text-center">{{session('error')}}</span>
										</div>
										@endif
										<span class="bmd-form-group">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">
														<i class="material-icons">email</i>
													</span>
												</div>
												{{-- <input type="text" name="email" class="form-control" placeholder="El.pašto adresas" value="{{ old('email') }}" /> --}}
												<input type="text" name="email" class="form-control" placeholder="Email" />
												
											</div>
											@if($errors->has('email'))
											<div class="text-danger login-error-message">{{ $errors->first('email') }}</div>
											@endif
										</span>
										<span class="bmd-form-group">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">
														<i class="material-icons">lock_outline</i>
													</span>
												</div>
												{{-- <input type="password" name="password" class="form-control" placeholder="Slaptazodis" value="{{ old('password') }}"> --}}
												<input type="password" name="password" class="form-control" placeholder="Password">
											
											</div>
											@if($errors->has('password'))
											<div class="text-danger login-error-message">{{ $errors->first('password') }}</div>
											@endif
										</span>
									</div>
									<div class="card-footer justify-content-center">
										<button type ="submit" class="btn fontcolor btn-link btn-lg">LOG IN</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<script src="{{url('assets/admin/js/core/jquery.min.js')}}"></script>
		<script src="{{url('assets/admin/js/core/popper.min.js')}}"></script>
		<script src="{{url('assets/admin/js/core/bootstrap-material-design.min.js')}}"></script>
		<script src="{{url('assets/admin/js/core/perfect-scrollbar.jquery.min.js')}}"></script>
		<script src="{{url('assets/admin/js/core/material-dashboard.min.js')}}"></script>
		<script src="{{url('assets/admin/js/core/demo.js')}}"></script>		  
		<script>
		$(document).ready(function() {
			md.checkFullPageBackgroundImage();
			setTimeout(function() {
				$('.card').removeClass('card-hidden');
			}, 700);
		});
		</script>
	</body>

</html>