<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Employee Panel
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" rel="stylesheet" type="text/css"/>
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">

  <link href="{{ URL('assets/admin/css/material-dashboard.min.css?v=2.1.2') }}" rel="stylesheet"/>
  <link href="{{ URL('assets/admin/demo/demo.css') }}" rel="stylesheet"/>
  <link href="{{ URL('assets/admin/css/glyphicons.css') }}" rel="stylesheet"/>  
  {{-- tags --}}
  <link href="{{ URL('assets/admin/css/tags.css') }}" rel="stylesheet"/>
  <link href="{{ URL('assets/admin/css/toastr.min.css') }}" rel="stylesheet"/>

</head>
  
<body class="">
  
  <div class="wrapper ">
    <div class="sidebar" data-color="green" data-background-color="black" data-image="{{url ('assets/admin/img/sidebar-1.jpg') }}">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo"><a href="#" class="simple-text logo-mini">
          EP
        </a>
        <a href="#" class="simple-text logo-normal">
          Employee Panel
        </a></div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item">
            
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="{{route('employees.index')}}">
              <i class="fa fa-users"></i>
              <p> Employees
              </p>
            </a>
          </li>
         

          <li class="nav-item ">
            <a class="nav-link" href="{{route('adminLogout')}}">
                <i class="fa fa-sign-out" aria-hidden="true"></i>
                <p>Log out </p>
            </a>
        </li>
          
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
          <div class="container-fluid">
              <div class="navbar-wrapper">
                  <div class="navbar-minimize">
                      {{-- <button class="btn btn-just-icon btn-white btn-fab btn-round" id="minimizeSidebar">
                          <i class="material-icons text_align-center visible-on-sidebar-regular">
                              more_vert
                          </i>
                          <i class="material-icons design_bullet-list-67 visible-on-sidebar-mini">
                              view_list
                          </i>
                      </button> --}}
                  </div>
                  {{-- <a class="navbar-brand" href="javascript:;">
                      Dashboard
                  </a> --}}
              </div>
              <button aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-toggle="collapse" type="button">
                  <span class="sr-only">
                      Toggle navigation
                  </span>
                  <span class="navbar-toggler-icon icon-bar">
                  </span>
                  <span class="navbar-toggler-icon icon-bar">
                  </span>
                  <span class="navbar-toggler-icon icon-bar">
                  </span>
              </button>
              <div class="collapse navbar-collapse justify-content-end">
                  <ul class="navbar-nav">
                      <li class="nav-item dropdown">
                          <a aria-expanded="false" aria-haspopup="true" class="nav-link" data-toggle="dropdown" href="javascript:;" id="navbarDropdownProfile">
                              <i class="material-icons">
                                  person
                              </i>
                              <p class="d-lg-none d-md-block">
                                  Account
                              </p>
                          </a>
                          <div aria-labelledby="navbarDropdownProfile" class="dropdown-menu dropdown-menu-right">
                              <div>
                              </div>
                              <a class="dropdown-item" href="{{route('adminLogout')}}">
                                  Log out
                              </a>
                          </div>
                      </li>
                  </ul>
              </div>
          </div>
      </nav>
      <div class="content">
          <div class="content">
              <div class="container-fluid">
                  @yield('content')
              </div>
          </div>
      </div>
  </div>


      <footer class="footer">
        <div class="container-fluid">
          <nav class="float-left">
            <ul>
              <li>
                <a href="#">
                  Priyanka Mehta
                </a>
              </li>
              
            </ul>
          </nav>
          <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script>, Designed by
            <!-- <i class="material-icons">favorite</i> by -->
            <a href="#" >Priyanka Mehta</a> for a better web.
          </div>
        </div>
      </footer>
    </div>
  </div>

  <!--   Core JS Files   -->
  <script src="{{ URL('/assets/admin/js/core/jquery.min.js') }}"></script>
    <script src="{{ URL('/assets/admin/js/core/popper.min.js') }}"></script>
    <script src="{{ URL('/assets/admin/js/core/bootstrap-material-design.min.js') }}"></script>
    <script src="{{ URL('/assets/admin/js/core/perfect-scrollbar.jquery.min.js') }}"></script>
    <script src="{{ URL('/assets/admin/js/core/tags.js') }}"></script>
    <script src="{{ URL('/assets/admin/js/core/toastr.min.js') }}"></script>
    <script src="{{ URL('/assets/admin/js/custom.js') }}"></script>
    <!-- Plugin for the momentJs  -->
    <script src="{{ URL('/assets/admin/js/core/moment.min.js') }}"></script>
    <!--  Plugin for Sweet Alert -->
    <script src="{{ URL('/assets/admin/js/core/sweetalert.min.js') }}"></script>
    <!-- Forms Validations Plugin -->
    {{-- <script src="{{ URL('/assets/admin/js/core/jquery.validate.min.js') }}"></script> --}}
    <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
    <script src="{{ URL('/assets/admin/js/core/jquery.bootstrap-wizard.js') }}"></script>
    <!--   Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
    <script src="{{ URL('/assets/admin/js/core/bootstrap-selectpicker.js') }}"></script>
    <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
    <script src="{{ URL('/assets/admin/js/core/bootstrap-datetimepicker.min.js') }}"></script>
    <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
    <!--  <script src="{{ URL('/assets/admin/js/core/plugins/jquery.dataTables.min.js') }}"></script> -->
    <script src="{{ URL('/assets/admin/js/core/jquery.dataTables.min.js') }}"></script>
    <!--   Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
    {{-- <script src="{{ URL('/assets/admin/js/core/bootstrap-tagsinput.js') }}"></script> --}}
    <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
    <script src="{{ URL('/assets/admin/js/core/jasny-bootstrap.min.js') }}"></script>
    <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
    <script src="{{ URL('/assets/admin/js/core/fullcalendar.min.js') }}"></script>
    <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
    <script src="{{ URL('/assets/admin/js/core/jquery-jvectormap.js') }}"></script>
    <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
    <script src="{{ URL('/assets/admin/js/core/nouislider.min.js') }}"></script>
    <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs-js/core/2.4.1.js"></script> --}}
    <!-- Library for adding dinamically elements -->
    <script src="{{ URL('/assets/admin/js/core/arrive.min.js') }}"></script>
    <!--  Google Maps Plugin    -->
    <!-- Place this tag in your head or just before your close body tag. -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>
    <!-- Chartist JS -->
    <script src="{{ URL('assets/admin/js/core/chartist.min.js') }}"></script>
    <!--  Notifications Plugin    -->
    <script src="{{ URL('assets/admin/js/core/bootstrap-notify.js') }}"></script>
    <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="{{ URL('assets/admin/js/core/material-dashboard.min.js') }}"></script>
    <!-- Material Dashboard DEMO methods, don't include it in your project! -->
    <script src="{{ URL('assets/admin/demo/demo.js') }}"></script>
    <script src="{{ URL('assets/admin/js/core/jquery.sharrre.js') }}"></script>
    {{-- <script src="http://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script> --}}
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
  <script>
    $(document).ready(function() {
      $().ready(function() {
        $sidebar = $('.sidebar');

        $sidebar_img_container = $sidebar.find('.sidebar-background');

        $full_page = $('.full-page');

        $sidebar_responsive = $('body > .navbar-collapse');

        window_width = $(window).width();

        fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

        if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
          if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
            $('.fixed-plugin .dropdown').addClass('open');
          }

        }

        $('.fixed-plugin a').click(function(event) {
          // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
          if ($(this).hasClass('switch-trigger')) {
            if (event.stopPropagation) {
              event.stopPropagation();
            } else if (window.event) {
              window.event.cancelBubble = true;
            }
          }
        });

        $('.fixed-plugin .active-color span').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-color', new_color);
          }

          if ($full_page.length != 0) {
            $full_page.attr('filter-color', new_color);
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.attr('data-color', new_color);
          }
        });

        $('.fixed-plugin .background-color .badge').click(function() {
          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('background-color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-background-color', new_color);
          }
        });

        $('.fixed-plugin .img-holder').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).parent('li').siblings().removeClass('active');
          $(this).parent('li').addClass('active');


          var new_image = $(this).find("img").attr('src');

          if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            $sidebar_img_container.fadeOut('fast', function() {
              $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
              $sidebar_img_container.fadeIn('fast');
            });
          }

          if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $full_page_background.fadeOut('fast', function() {
              $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
              $full_page_background.fadeIn('fast');
            });
          }

          if ($('.switch-sidebar-image input:checked').length == 0) {
            var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
            $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
          }
        });

        $('.switch-sidebar-image input').change(function() {
          $full_page_background = $('.full-page-background');

          $input = $(this);

          if ($input.is(':checked')) {
            if ($sidebar_img_container.length != 0) {
              $sidebar_img_container.fadeIn('fast');
              $sidebar.attr('data-image', '#');
            }

            if ($full_page_background.length != 0) {
              $full_page_background.fadeIn('fast');
              $full_page.attr('data-image', '#');
            }

            background_image = true;
          } else {
            if ($sidebar_img_container.length != 0) {
              $sidebar.removeAttr('data-image');
              $sidebar_img_container.fadeOut('fast');
            }

            if ($full_page_background.length != 0) {
              $full_page.removeAttr('data-image', '#');
              $full_page_background.fadeOut('fast');
            }

            background_image = false;
          }
        });

        $('.switch-sidebar-mini input').change(function() {
          $body = $('body');

          $input = $(this);

          if (md.misc.sidebar_mini_active == true) {
            $('body').removeClass('sidebar-mini');
            md.misc.sidebar_mini_active = false;

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

          } else {

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

            setTimeout(function() {
              $('body').addClass('sidebar-mini');

              md.misc.sidebar_mini_active = true;
            }, 300);
          }

          // we simulate the window Resize so the charts will get updated in realtime.
          var simulateWindowResize = setInterval(function() {
            window.dispatchEvent(new Event('resize'));
          }, 180);

          // we stop the simulation of Window Resize after the animations are completed
          setTimeout(function() {
            clearInterval(simulateWindowResize);
          }, 1000);

        });
      });
    });
  </script>
  <!-- Sharrre libray -->
  <script src="{{ URL ('assets/admin/js/core/jquery.sharrre.js') }}"></script>
  <script>
    $(document).ready(function() {


      $('#facebook').sharrre({
        share: {
          facebook: true
        },
        enableHover: false,
        enableTracking: false,
        enableCounter: false,
        click: function(api, options) {
          api.simulateClick();
          api.openPopup('facebook');
        },
        template: '<i class="fab fa-facebook-f"></i> Facebook',
        url: 'examples/dashboard.html'
      });

      $('#google').sharrre({
        share: {
          googlePlus: true
        },
        enableCounter: false,
        enableHover: false,
        enableTracking: true,
        click: function(api, options) {
          api.simulateClick();
          api.openPopup('googlePlus');
        },
        template: '<i class="fab fa-google-plus"></i> Google',
        url: 'examples/dashboard.html'
      });

      $('#twitter').sharrre({
        share: {
          twitter: true
        },
        enableHover: false,
        enableTracking: false,
        enableCounter: false,
        buttons: {
          twitter: {
            via: 'CreativeTim'
          }
        },
        click: function(api, options) {
          api.simulateClick();
          api.openPopup('twitter');
        },
        template: '<i class="fab fa-twitter"></i> Twitter',
        url: 'examples/dashboard.html'
      });


      // Facebook Pixel Code Don't Delete
      ! function(f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function() {
          n.callMethod ?
            n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
      }(window,
        document, 'script', 'http://connect.facebook.net/en_US/fbevents.js');

      try {
        fbq('init', '111649226022273');
        fbq('track', "PageView");

      } catch (err) {
        console.log('Facebook Track Error:', err);
      }

    });
  </script>
  <script>
    // Facebook Pixel Code Don't Delete
    ! function(f, b, e, v, n, t, s) {
      if (f.fbq) return;
      n = f.fbq = function() {
        n.callMethod ?
          n.callMethod.apply(n, arguments) : n.queue.push(arguments)
      };
      if (!f._fbq) f._fbq = n;
      n.push = n;
      n.loaded = !0;
      n.version = '2.0';
      n.queue = [];
      t = b.createElement(e);
      t.async = !0;
      t.src = v;
      s = b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t, s)
    }(window,
      document, 'script', 'http://connect.facebook.net/en_US/fbevents.js');

    try {
      fbq('init', '111649226022273');
      fbq('track', "PageView");

    } catch (err) {
      console.log('Facebook Track Error:', err);
    }
  </script>
  <noscript>
    <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=111649226022273&amp;ev=PageView&amp;noscript=1" />
  </noscript>
  <script>
    $(document).ready(function() {
      // Javascript method's body can be found in assets/js/demos.js
      md.initDashboardPageCharts();

      md.initVectorMap();

    });
  </script>
  @yield('js')
</body>


<!-- Mirrored from demos.creative-tim.com/material-dashboard-pro/examples/dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 06 Aug 2020 06:56:02 GMT -->
</html>