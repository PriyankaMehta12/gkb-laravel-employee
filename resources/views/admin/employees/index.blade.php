@extends('admin.layout')
@section('content')

<div class="content">
   <div class="container-fluid">
      <div class="card-body">
         <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data">
             @csrf
             <input type="file" name="file" class="form-control">
             <br>
             <button class="btn btn-success">Import Employee Data</button>
         </form>
     </div>
      <div class="row">
         <div class="col-md-12">
            <div class="card">
               <div class="card-header card-header-primary card-header-icon">
                  <div class="card-icon">
                    <i class="fa fa-user fa-2x"></i>
                  </div>
                  <h4 class="card-title mb-0 font-weight-bold">Employee</h4>
               </div>
               <div class="card-body">
                <div class="row">
                    <div class="col-md-12">   
                      <a href="{{route('employees.create')}}" class="btn btn-primary pull-right font-weight-bold chk-btn" title="Add">Add</a>    
                    </div>
                  </div>
                  <div class="material-datatables">
                     <table id="employee-datatable" class="table table-bordered data-table" cellspacing="0" width="100%">
                        <thead>
                           <tr class="border">
                                <th class="font-weight-bold">Id</th>
                                <th class="font-weight-bold">First Name</th>
                                <th class="font-weight-bold">Last Name</th>
                                <th class="font-weight-bold">Email</th>
                                <th class="font-weight-bold">Hobbies</th>
                                <th class="font-weight-bold">Gender</th>
                                <th class="font-weight-bold">Image</th>
                                <th class="font-weight-bold">Action</th>
                           </tr>
                        </thead>
                        <tfoot>
                            <tr class="border">
                                <th class="font-weight-bold">Id</th>
                                <th class="font-weight-bold">First Name</th>
                                <th class="font-weight-bold">Last Name</th>
                                <th class="font-weight-bold">Email</th>
                                <th class="font-weight-bold">Hobbies</th>
                                <th class="font-weight-bold">Gender</th>
                                <th class="font-weight-bold">Image</th>
                                <th class="font-weight-bold">Action</th>
                            </tr>
                        </tfoot>
                        <tbody>                  
                           {{-- body --}}
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection

@section('js')
  <script type="text/javascript">
    $(function () {
      var table = $('#employee-datatable').DataTable({  
          processing: true,
          serverSide: true,
          ajax: "{{ route('employees.index') }}",  
          columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: true, searchable: true},  
              {data: 'first_name', name: 'last_name'},
              {data: 'last_name', name: 'last_name'},
              {data: 'email', name: 'email'},
              {data: 'hobbies', name: 'hobbies', orderable: false},
              {data: 'gender', name: 'gender', orderable: false},
              {data: 'image', name: 'image', orderable: false},
              {data: 'action', name: 'action', orderable: false, searchable: false},
          ]
      });   
    });
  </script>
@endsection