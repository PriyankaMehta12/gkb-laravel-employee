@extends('admin.layout')
@section('content')

<div class="content">
<div class="container-fluid">
   <div class="row">
      <div class="col-md-12">
         <div class="card">
            <div class="card-header card-header-primary card-header-icon">
               <div class="card-icon">
                  <i class="fa fa-user fa-2x"></i>
               </div>
               <h4 class="card-title mb-0 font-weight-bold"> Add Employees</h4>
            </div>
            <div class="card-body">
               <form class="design-form1" id="employee1" method="POST" action="{{ route('employees.store') }}" enctype="multipart/form-data">
                  @csrf
                  <br>
                  <div class="row">
                     <div class="col-md-6">
                        <label class="font-weight-bold">Enter First Name</label>
                        <div class="form-group">                                        
                           <input type="text" name="first_name" id="first_name" class="form-control" placeholder="Enter First Name">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <label class="font-weight-bold">Enter Last Name</label>
                        <div class="form-group">                                        
                           <input type="text" name="last_name" id="last_name" class="form-control" placeholder="Enter Last Name">
                        </div>
                     </div>
                     
                  </div>
                  <br>
                  <div class="row">
                    <div class="col-md-6">
                        <label class="font-weight-bold">Enter Email</label>
                        <div class="form-group">                                        
                           <input type="text" name="email" id="email" class="form-control" placeholder="Enter Email">
                        </div>
                     </div>
                
                     <div class="col-md-6">
                        <label class="font-weight-bold">Hobbies</label>
                        <label>
                          <input type="checkbox" class="radio" value="tv" name="hobby[]" />TV</label>
                        <label>
                          <input type="checkbox" class="radio" value="reading" name="hobby[]" />Reading</label>
                        <label>
                          <input type="checkbox" class="radio" value="coding" name="hobby[]" />Coding</label>
                        <label>
                          <input type="checkbox" class="radio" value="skiing" name="hobby[]" />Skiing</label>
                     </div>
                 </div>
                 <br>
                 <div class="row">
                    {{-- <div class="col-md-6">
                        <label for="gender" class="form-label">Gender</label>
                        <select class="form-select" id="gender" name="gender" >
                            <option value="" selected disabled>Gender</option>
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                        </select>
                    </div> --}}
                    <div class="col-md-6">
                        <div class="radio-section">
                            <label for="date">Gender : </label>
                            <ul class="radio-buttons-w3-agileits">
                                <li>
                                    <input type="radio" id="gender" value="male"
                                        name="gender">
                                    <label for="male">male</label>
                                    <div class="check"></div>
                                </li>
                                <li>
                                    <input type="radio" id="gender" value="female"
                                        name="gender">
                                    <label for="female">female</label>
                                    <div class="check">
                                        <div class="inside"></div>
                                    </div>
                                </li>
                            </ul>

                            <div class="clear"></div>
                        </div>
                    </div>
                     <div class="col-md-6">
                        <label for="profile" class="form-label">Profile Picture</label>
                        <input type="file" class="form-control" name="image" id="image">
                    </div>
                 </div>
                  <div class="form-group text-right">
                    <button type="submit" class="btn btn-primary font-weight-bold chk-btn" title="Add Now">Add Now</button>
               </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
   $( document ).ready(function() {
       $("#employee1").validate({     
           rules:{
               first_name:{
                   required:true
               },
               last_name:{
                   required:true
               },
               email:{
                   email:true
               },         
           },
           messages:{
               first_name:{
                   required:"First Name field is required."
               },
               last_name:{
                   required:"Last Name field is required."
               },
               email:{
                   email:"Invalid email format."
               },
           },        
       });
   });    
</script>
@endsection
