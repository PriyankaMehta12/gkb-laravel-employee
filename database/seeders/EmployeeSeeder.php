<?php

namespace Database\Seeders;
use App\Models\Employee;
use Illuminate\Database\Seeder;
use Str;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 101; $i++) {
            $employeeData[] = [
                'first_name' => Str::random(10),
                'last_name' => Str::random(10),
                'email' => Str::random(10).'@gmail.com',
                'hobbies' => 'tv, reading',
                'gender' => 'female',
                'image' => 'uploads/2021/09/6146e7efa1a79.jpg',
            ];
        }

        foreach ($employeeData as $employee) {
            Employee::create($employee);
        }
        
        // Employee::insert([
        //     [
        //         'first_name' => 'empta', 
        //         'last_name' => 'empta', 
        //         'email' => 'emp111werw@gmail.com',
        //         'hobbies' => 'tv,reading',
        //         'gender' => 'female',
        //         'image' => 'uploads/2021/09/6146e7efa1a79.jpg',
        //     ],
        //     [
        //         'first_name' => 'empndra', 
        //         'last_name' => 'empta', 
        //         'email' => 'emp2werw@gmail.com',
        //         'hobbies' => 'tv,reading',
        //         'gender' => 'female',
        //         'image' => 'uploads/2021/09/6146e7efa1a79.jpg',
        //     ],
        //     [
        //         'first_name' => 'empa', 
        //         'last_name' => 'empta', 
        //         'email' => 'emp3werw@gmail.com',
        //         'hobbies' => 'tv,reading',
        //         'gender' => 'female',
        //         'image' => 'uploads/2021/09/6146e7efa1a79.jpg',
        //     ],
        //     [
        //         'first_name' => 'emp', 
        //         'last_name' => 'empta', 
        //         'email' => 'emp4werw@gmail.com',
        //         'hobbies' => 'tv,reading',
        //         'gender' => 'female',
        //         'image' => 'uploads/2021/09/6146e7efa1a79.jpg',
        //     ],
        //     [
        //         'first_name' => 'empshiya', 
        //         'last_name' => 'empta', 
        //         'email' => 'emp5b@matrixmob.com',
        //         'hobbies' => 'tv,reading',
        //         'gender' => 'female',
        //         'image' => 'uploads/2021/09/6146e7efa1a79.jpg',
        //     ],
        // ]);
    }
}
